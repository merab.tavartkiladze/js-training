#!/usr/bin/node

//
// ./ans.mjs  <zip-folder-location> <zip-file-location-name>
// ONLY help from https://nodejs.org/api/

/** Import generics dependences */

import fs from 'fs';
import tar from 'tar-pack';

const srcFolder = process.argv[2];
const dstFile = process.argv[3];

const write = fs.createWriteStream(dstFile);
const pack = tar.pack(srcFolder);

pack.pipe(write);

write.on('open', () => console.log(`Event: ${dstFile} open`));
write.on('ready', () => console.log(`Event: ${dstFile} ready`));
write.on('finish', () => console.log(`Event: ${dstFile} finish`));
write.on('close', () => console.log(`Event: ${dstFile} close`));
write.on('error', () => console.log(`Event: ${dstFile} error`));